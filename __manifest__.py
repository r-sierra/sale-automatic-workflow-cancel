# -*- coding: utf-8 -*-
{
    'name': 'Sale Automatic Workflow Cancel',
    'summary': 'Adds an automatic action to workflow to allow orders cancelation',
    'author': 'Roberto Sierra <roberto@ideadigital.com.ar>',
    'website': 'https://gitlab.com/r-sierra/sale-automatic-workflow-cancel',
    'category': 'Sales Management',
    'license': 'AGPL-3',
    'version': '13.0.0.1.0',
    'depends': [
        'sale_automatic_workflow',
    ],
    'data': [
        'views/sale_workflow_process_view.xml',
    ],
}
