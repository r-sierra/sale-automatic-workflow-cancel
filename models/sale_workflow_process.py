# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import fields, models


class SaleWorkflowProcess(models.Model):
    """ A workflow process is the setup of the automation of a sales order.

    Each sales order can be linked to a workflow process.
    Then, the options of the workflow will change how the sales order
    behave, and how it is automatized.
    """

    _inherit = "sale.workflow.process"

    cancel_order = fields.Boolean(
        string="Cancel Order",
        help="If set, orders will be canceled if not confirmed after 'Cancelation Days' have passed since its creation"
    )
    cancel_order_filter_domain = fields.Text(
        string="Cancel Order Filter Domain",
        related="cancel_order_filter_id.domain"
    )
    cancel_order_filter_id = fields.Many2one(
        "ir.filters",
        string="Cancel Order Filter",
        default=lambda self: self._default_filter(
            "sale_automatic_workflow.automatic_workflow_order_filter"
        ),
    )
    cancel_days = fields.Integer(
        string="Days to cancel",
        help="The number of days after creation to cancel draft orders",
    )
