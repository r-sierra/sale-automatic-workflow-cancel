# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import logging

from odoo import api, fields, models
from odoo.tools.safe_eval import safe_eval
from odoo.tools.date_utils import subtract
from odoo.addons.sale_automatic_workflow.models.automatic_workflow_job import savepoint, force_company

_logger = logging.getLogger(__name__)


class AutomaticWorkflowJob(models.Model):
    """ Scheduler that will play automatically the validation of
    invoices, pickings...  """

    _inherit = "automatic.workflow.job"

    def _do_cancel_sale_order(self, sale, domain_filter):
        """Set a sales order to cancel, filter ensure no duplication"""
        if not self.env["sale.order"].search_count(
            [("id", "=", sale.id)] + domain_filter
        ):
            return "{} {} job bypassed".format(sale.display_name, sale)
        sale.action_cancel()
        return "{} {} set cancel successfully".format(sale.display_name, sale)

    @api.model
    def _cancel_sale_orders(self, cancel_order_filter):
        sale_obj = self.env["sale.order"]
        sales = sale_obj.search(cancel_order_filter)
        _logger.debug("Sale Orders to cancel: %s", sales.ids)
        for sale in sales:
            with savepoint(self.env.cr), force_company(self.env, sale.company_id):
                self._do_cancel_sale_order(sale, cancel_order_filter)

    @api.model
    def run_with_workflow(self, sale_workflow):
        super(AutomaticWorkflowJob, self).run_with_workflow(sale_workflow)

        if sale_workflow.cancel_order and sale_workflow.cancel_days:
            current_date = fields.Date.context_today(self)
            workflow_domain = [
                ("workflow_process_id", "=", sale_workflow.id),
                ("date_order", "<", subtract(current_date, days=sale_workflow.cancel_days)),
            ]
            self._cancel_sale_orders(
                safe_eval(sale_workflow.cancel_order_filter_id.domain) + workflow_domain
            )
